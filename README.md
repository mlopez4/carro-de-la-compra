# Carro de la compra

# INDEX

- Introducció
- Diagrames
- Diagrames de casos d&#39;us
- Diagrames de estats
- Diagrama de classes
- Documentació
- Punts d&#39;interès
- Punts a millorar

# INTRODUCCIÓ

Basat en següent enunciat proporcionat per el professor s&#39;ha desenvolupat un petit aplicatiu que simula un carro de la compra de una botiga virtual:

[http://moodlecf.sapalomera.cat/pluginfile.php/25713/mod\_assign/introattachment/0/DAW-M3-M5-UF3-UF4-Pt01\_v2.0.docx?forcedownload=1](http://moodlecf.sapalomera.cat/pluginfile.php/25713/mod_assign/introattachment/0/DAW-M3-M5-UF3-UF4-Pt01_v2.0.docx?forcedownload=1)

Tot hi que l&#39;aplicatiu s&#39;ha dividit en una part de gestió d&#39;estoc (administrador/propietari) i una altra de gestió del carro de compra (usuari/client) s&#39;ha omes la part d&#39;autentificació per no complicar mes del necessari la exposició i presentació del projecte. Es per això que l&#39;usuari disposarà de les dues seccions en el mateix entorn de treball sense necessitat d&#39;identificar-se en el sistema.

S&#39;ha fet servir una arquitectura MVC (Model Vista Controlador) per la seva implementació, el qual s&#39;explica a continuació:

 El nostre **model** de dades gestiona un arxiu (estoc.csv) on tenim emmagatzemat el nostre estoc de productes disponibles a la nostra botiga. Aquest fitxer es carrega sencer a l&#39;aplicatiu (a un ArrayList) mitjançant un dels constructor de la classe _Contenidor_. La nostra classe _Contenidor_ disposa de dos mètodes constructors un per crear un estoc a partir del nostre fitxer csv i l&#39;altre per crear el nostre carro de la compra  buit.  Els notres objectes _estoc_ i _carro_ interactuen entre ells mantenint sempre la consistencia mutua de les dades. Després de realitzar les operacions necessaries el sistema torna a fer un volcatje de dades sobreescrivint l&#39;arxiu inicial(estoc.csv) o sobreescrivint l&#39;arxiu &quot;ultimaVenda.csv&quot; en el cas del nostre carro.

 ![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/Model%20de%20dades.bmp)
 
El model de **vista** consta d&#39;una classe _Vista_ on tenim diferents funcions que printen en pantalla els diferents menus del nostre aplicatiu i retornen l&#39;opció escollida per l&#39;usuari, axí com la mostra del tiquet per pantalla i el nostre estoc. S&#39;ajuda d&#39;algunes funcions de la clase _Validacio_ que forma part de la secció **controlador** que s&#39;explica a continuacó.

En l&#39;apartat de **control** tenim la classe _Validacio_ que s&#39;encarrega de la validació de les dades entrades per l&#39;usuari i la classe principal _Main_ que es l&#39;encarregada d&#39;anar cridant els diferents menús de la classe Vista depenent de la elecció de l&#39;usuari mitjançant estructures de flux (switch niats entre ells) i de les diferents funcions que gestionen les interrelacions entre _l&#39;estoc_ i el _carro._



# DIAGRAMES UML

- Diagrama de casos d&#39;us:

 ![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/Diagrama%20us%20carro%20compra.bmp)
 
| **CAS D&#39;US:** | Gestionar carreto de la compra |
| --- | --- |
| **Versió:** | Versió usuari |
| **Descripció:** | Un usuari emplena el seu carro de la compra en un supermercat |
| **Actors:** | Usuari |
| **Precondició:** | L&#39;usuari ja esta registrat en el sistema com a usuari. |
| **Flux principal:** | 1.L&#39;usuari escull els productes que vol afegir o treure del seu carro (Prèviament sempre es mostra un llistat del productes en estoc). 1.1.S&#39;actualitza l&#39;estoc i carro. 2.L&#39;usuari passa per caixa. 2.1 El carro s&#39;esborra. |
| **Flux alternatiu:** | El producte introduït no es troba en l&#39;estocEs torna a demanar producte. |
| **Postcondició:** | Si el carro ja s&#39;ha esborrat accedim a una altra plataforma per el pagament. (fora del sistema a desenvolupar) |

| **CAS D&#39;US:** | Gestionar estoc |
| --- | --- |
| **Versió:** | Versió Administrador |
| **Descripció:** | Un administrador gestiona el estoc que tenim emmagatzemat en un fitxer csv |
| **Actors:** | Administrador |
| **Precondició:** | L&#39;usuari ja esta registrat com a usuari administrador |
| **Flux principal:** | 1.L&#39;usuari agrega o treu un producte de l&#39;estoc (prèviament  el sistema fa un llistat del productes emmagatzemats). 2.S&#39;actualitza l&#39;estoc.|
| **Flux alternatiu:** | 1.El producte no es troba a l&#39;estoc 2.Es crea un registre nou per al producte introduït |
| **Postcondició:** | S&#39;ha de mantenir la integritat de les dades del fitxer csv on es troba el nostre estoc. |



- Diagrama d&#39;estats:
 ![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/Estats%20generals.bmp)
Diagrama d&#39;estats:
Anex1 (Gestió d&#39;estoc):
![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/Estats%20estoc.bmp)
Diagrama d&#39;estats:
Anex 2 (Gestió del carro):
![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/Estats%20carro.bmp)

- Diagrama de classes:
 ![](https://gitlab.com/mlopez4/carro-de-la-compra/raw/master/res/3_Diagrames_imgs/classes.bmp)
 
# DOCUMENTACIO

Per documentar l&#39;aplicatiu s&#39;ha fet servir la versió de JavaDoc mes senzilla possible generada amb IntelliJ.

Es troba a:

[doc\index.html](https://gitlab.com/mlopez4/carro-de-la-compra/-/archive/master/carro-de-la-compra-master.zip)

# PUNTS D&#39;INTERÉS

S&#39;han considerat diversos punts del codi on s&#39;han aplicat algunes praxis les quals serien adient recalcar i explicar-les aïlladament:

- Per crear un producte a nivell de codi (objecte) ens ajudem dels constructors de cada tipus de productes que disposem (classes) on fiquem per paràmetre les característiques necessàries per el seu emmagatzematge en el nostre sistema.
- Els constructors anteriorment anomenats son els que automàticament apliquen la formula per calcular el preu definitiu en el cas dels productes d&#39;alimentació i tecnològics.
- L&#39;estoc i el carro es tracten com a objectes tipus &quot;contenidor&quot;, que es un objecte creat per gestionar tant una cosa com l&#39;altra amb el mateix tipus d&#39;objecte(classe). Aquet objecte nomes es un ArrayList encapsulat amb uns mètodes propis nostres per poder-lo gestionar externament des de altres classes.
- La classe vista te varis mètodes privats que fem servir com hienes que creen títols i línies de separació per ser implementades en les funcions que printen els menús de la mateixa classe.

# PUNTS A MILLORAR

A part de la implementació d&#39;un sistema de identificació de usuaris per diferenciar els accessos de administradors i clients també hi haurien altres possibles millores substancials com:

- Optimitzar la gestió de la funció &quot;demanarValidarEntrada&quot; de la classe _Validacio_, ja que ara es fa servir tant per demanar una opció com per demanar un enter. Seria més òptim fer dues funcions, una per cada cas.
- Fer servir un sistema senzill de base de dades. Tot hi que en el nostre petit cas no seria rellevant, si mes endavant l&#39;estoc de la botiga augmenta, el fet de bolcar fitxer de dades senceres al sistema suposaria un problema de rendiment.
- En la classe Producte s&#39;ha fet un mètode &quot;setCodiId&quot; per que a ultima horaens hem descuidat de afegir el paràmetre adequat al constructor de la classe per afegir-li el codi identificador. Cosa de la que es pot prescindir ja que no es fa servir en cap lloc de l&#39;aplicatiu.