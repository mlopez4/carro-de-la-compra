/**
 * @author Marcos Lopez Capitan
 */

package model;

/**
 * Classe per productes de tipus textil, hereta de Producte
 */
public class Textil extends Producte{

  /**
   * Composició del nostre producte
   */
  private String composicio;

  /**
   * Construeix un objecte textil amb les dades entrades per paràmetre
   * @param codiBarres codi de barres
   * @param nom nom del nostre producte
   * @param preuBase preu base del producte
   * @param unitats unitats inicials
   * @param composicio composicio
   */
  public Textil(String codiBarres, String nom, Double preuBase, int unitats, String composicio){
    this.codiBarres = codiBarres;
    this.nom = nom;
    this.preuBase = preuBase;
    this.preuActual = preuBase;
    this.unitats = unitats;
    this.composicio = composicio;
  }

  /**
   * Ens retorna la composició del producte
   * @return
   */
  public String getComposicio() {
    return composicio;
  }
}
