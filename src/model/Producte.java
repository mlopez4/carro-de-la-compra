/**
 * @author Marcos Lopez Capitan
 */

package model;

/**
 * Classe abstracta, super classe d'on deriven tots els nostres productes.
 */
public class Producte {

  protected int codiId;
  protected String codiBarres;
  protected String nom;
  protected double preuActual;
  protected double preuBase;
  protected int unitats;

  /**
   * Introdueix un codi identificatori al producte
   * @param codiId
   */
  public void setCodiId(int codiId) {
    this.codiId = codiId;
  }

  /**
   * Ens dona el codi identificatori del producte
   * @return
   */
  public int getCodiId() {
    return codiId;
  }

  /**
   * Ens dona el code de barres del producte
   * @return
   */
  public String getCodiBarres() {
    return codiBarres;
  }

  /**
   * Ens dona el nom del producte
   * @return
   */
  public String getNom() {
    return nom;
  }

  /**
   * Ens dona el preu calculat (segons la fórmula adequada) del producte
   * @return
   */
  public double getPreuActual() {
    return preuActual;
  }

  /**
   * Ens dona el preu base des d'on es calcula l'atribut preuActual
   * @return
   */
  public double getPreuBase() {
    return preuBase;
  }

  /**
   * Ens dona les unitats disponibles del producte
   * @return
   */
  public int getUnitats() {
    return unitats;
  }

  /**
   * Actualitza les unitats del producte
   * @param unitats nou valor d'unitats
   */
  public void setUnitats(int unitats) {
    this.unitats =  unitats;
  }
}
