/**
 * @author Marcos Lopez Capitan
 */

package model;

/**
 * Classe per productes de tipus electronic, hereta de Producte
 */
public class Electronic extends Producte {

  /**
   * Numero de dies de garanties que té el producte
   */
  private int garantiaDies;

  /**
   * Construeix un objecte Electronic a partir de les dades entrades per parámetre
   * @param codiBarres codi de barres
   * @param nom nom del producte
   * @param preuBase preu base del producte
   * @param unitats unitats inicial del producte
   * @param garantiaDies dies que te de garantia
   */
  public Electronic (String codiBarres, String nom, Double preuBase, int unitats, int garantiaDies){

    this.codiBarres = codiBarres;
    this.nom = nom;
    this.preuBase = preuBase;
    this.unitats = unitats;
    this.garantiaDies = garantiaDies;

    // fem servir el parametre garantiaDies per calcular el preu actualizat
    Double preuCalculat = preuBase + (preuBase*garantiaDies/365*0.1);
    // arrodonim a dos decimals
    this.preuActual = (double)Math.round(preuCalculat * unitats * 100d) / 100d;
  }


  /**
   * Retorna els dies de garantia dels que disposa el nostre producte
   * @return
   */
  public int getGarantiaDies() {
    return garantiaDies;
  }
}
