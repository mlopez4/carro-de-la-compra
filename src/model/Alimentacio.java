/**
 * @author Marcos Lopez Capitan
 */

package model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe per productes de tipus alimentació, hereta de Producte
 */
public class Alimentacio extends Producte{

  private Date dataCad;
  private String dataCadString;


  public Alimentacio(){

  }

  /**
   * Construeix un nou objecte alimentació a partir d'unes dades entrades per paràmetre.
   * @param codiBarres codi de barres del producte
   * @param nom nom del producte
   * @param preuBase preu base del producte (sense calcular amb la formula corresponent)
   * @param unitats unitats inicials del producte
   * @param dataCad data de caducitat del producte
   */
  public Alimentacio(String codiBarres, String nom, Double preuBase, int unitats, String dataCad) {
    this.codiBarres = codiBarres;
    this.nom = nom;
    this.preuBase = preuBase;
    this.unitats = unitats;

    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    try {
      this.dataCad = formatDate.parse(dataCad);
      Date actual = new Date(); // Data actual
      long diferenciaEn_ms = this.dataCad.getTime() - actual.getTime(); // Diferencia en msegons
      long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24); // Diferencia en dies
      this.preuActual = preuBase - preuBase * (1/(dias+1)) + (preuBase * 0.1);
      this.dataCadString = dataCad;
    } catch (Exception ex){
      System.out.println("Error en data caducitat producte");
    }



  }


  /**
   * Ens retorna la data de caducitat en forma de String
   * @return data de caducitat del producte
   */
  public String getDataCad() {
    return dataCadString;
  }
}
