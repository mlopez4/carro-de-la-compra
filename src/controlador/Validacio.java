/**
 * @author Marcos Lopez Capitan
 */

package controlador;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Classe encarregada de validar dades entrades per l'usuari
 */
public class Validacio {








  /**
   * Comprova que data de caducitat introduïda per paràmetre en forma de string (dd/MM/yyyy) no sigui menor que la data actual
   *
   * @return data ja validada o "0" si es una data incorrecta
   */
  public static String demanarValiarData(){
    Scanner in = new Scanner(System.in);
    String data = in.nextLine();
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    try{
      Date dProducte = formatDate.parse(data);
      Date actual = new Date();
      if ((dProducte.getTime() - actual.getTime())<0){
        System.out.println("No volem productes caducats!!");
        data = "0";
      }
    } catch(Exception e){
      System.out.println("El format ha de ser 'dd/MM/yyyy'");
      data = "0";
    }

    return data;
  }


  /**
   * Printa el missatge passat per paràmetre i demana a l'usuari introduir un text
   * @param msg missatge a mostrar
   * @return text entrat per l'usuari
   */
  public static String demanarCodiBarres(String msg){
    Scanner in = new Scanner(System.in);
    System.out.println(msg);
    return in.nextLine();
  }











  /**
   *  Demana una entrada per teclat i comprova que sigui un enter, també comprova que correspongui amb les opcions donades també per paràmetre en forma d'array d'enters
   *  Precondició: Els enters de l'array han de coincidir amb les opcions del menú que prèviament s'ha executat amb funcions de la classe vista.Vista
   *  Precondició: Si l'array del paràmetre es null interpreta que nomès volem comprovar si l'entrada es un enter.
   * @param opcions opcions disponibles
   * @return qualsevol de les opcions disponibles i en cas d'una entrada incorrecte retorna 0
   */
  public static int demanarValidarEntrada(int[] opcions){
    Scanner in = new Scanner(System.in);
    int sortida = 0;

    if (opcions == null){     // si NO hi han opcions
      if (in.hasNextInt()){ // si es enter llegim la dada
        sortida = in.nextInt();
      } else { // si NO es enter omitim la entrada i retornem 0
        in.next();
        sortida = 0;
      }
    } else {                 // si hi han opcions
      if (in.hasNextInt()){
        int entrada = in.nextInt();
        for (int i=0; i<opcions.length; i++){
          if (entrada == opcions[i]){
            sortida = entrada;
          }
        }
      } else {
        in.next();
        sortida = 0;
      }
    }
    return sortida;
  }









  /**
   * Demana i comprova un número decimal a l'usuari
   * @return numero ja validat
   */
  public static double demanarValidarDecimal(){
    Scanner leer = new Scanner(System.in);
    boolean esDouble = false;
    double num = -1;
    do {
      String cadena = leer.nextLine();
      try {
        num = Double.parseDouble(cadena);
        esDouble = true;
      } catch (NumberFormatException nfe) {
        System.out.println("Escriba un numero decimal");
      }
    } while (!esDouble);
    return num;
  }

}
