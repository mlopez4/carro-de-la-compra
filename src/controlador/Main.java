/**
 * @author Marcos Lopez Capitan
 */

package controlador;

import model.*;
import vista.Vista;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static Contenidor estoc = new Contenidor("res/estoc.csv");
    private static Contenidor carroActiu = new Contenidor();

    /**
     * Flux general de la aplicació
     * @param args
     */
    public static void main(String[] args) {

        boolean sortir = false;
        while (sortir == false) {
            switch (Vista.showMenuPrincipal()) {

                // Gestio destoc
                case 1:
                    boolean sortirtEstoc = false;
                    while (sortirtEstoc == false){
                        Vista.showEstoc(estoc);
                        switch (Vista.showMenuGestioEstoc()){
                            case 1: // Afegir producte a estoc
                                String codiInt = Validacio.demanarCodiBarres("Introdueix el codi de barres del producte a afegir:");
                                afegirProducteEstoc(codiInt);
                                break;
                            case 2: // Treure producte del estoc
                                String codiRd = Validacio.demanarCodiBarres("Introdueix el codi de barres del producte a borrar:");
                                treureProducteContenidor(estoc, codiRd);
                                break;
                            case 3: // Tornar
                                estoc.actualitzarFitxerEstoc("res/estoc.csv");
                                sortirtEstoc = true;
                                break;
                        }
                    }
                    break;

                // Gestio de carro
                case 2:
                    boolean sortirCarro = false;
                    while(sortirCarro == false){
                        Vista.showCarro(carroActiu);
                        switch (Vista.showMenuGestioCarro()){
                            case 1: // Afegir producte
                                Vista.showEstoc(estoc);
                                String codiInt = Validacio.demanarCodiBarres("Introdueix el codi de barres del producte a afegir:");
                                afegirProducteCarro(codiInt);
                                break;
                            case 2: // Borrar producte
                                Vista.showCarro(carroActiu);
                                String codiRd = Validacio.demanarCodiBarres("Introdueix el codi de barres del producte a borrar:");
                                int unitatsTreure = treureProducteContenidor(carroActiu, codiRd);
                                retornarProducteEstoc(codiRd,unitatsTreure);
                                eliminaProducteSenseUnitats(carroActiu);
                                break;
                            case 3: // Tornar
                                sortirCarro = true;
                                break;
                        }
                    }
                    break;

                // Passar per caixa
                case 3:
                    if (Vista.showTiquet(carroActiu)){
                        estoc.actualitzarFitxerEstoc("res/estoc.csv");
                        carroActiu.actualitzarFitxerEstoc("res/ultimaVenda.csv");
                        ArrayList<Producte> carro = carroActiu.getCarro();
                        carro.clear();
                    };
                    break;

                // Sortir
                case 4:
                    sortir = true;
                    break;


            }

        }
    }












    /**
     * Afegeix un producte amb el codi passat per paràmetre a l'estoc de la classe Main
     * També comprova si ja hi ha un producte amb aquest codi, si fos així demana les unitats a incrementar
     * si no existeix, demana les dades per registrar un producte nou
     * @param codi codi de barres
     */
    private static void afegirProducteEstoc(String codi){

        boolean trobat = false;
        ArrayList<Producte> productesStoc = estoc.getCarro();

        for (int i=0; i<productesStoc.size(); i++){
            if (codi.equalsIgnoreCase(productesStoc.get(i).getCodiBarres())){  //Si troba un producte amb el mateix codi...
                trobat = true;
                System.out.println("El producte ja esta en estoc.");
                System.out.println("Quantes unitats vols afegir?");
                System.out.println("(També es processarán valors negatius)");

                // demanem les unitats a afegir (si son negatives es tornen a demanar)
                int unitats = 0;
                do {
                    int[] tmp = null;
                    unitats = Validacio.demanarValidarEntrada(tmp);
                    if(unitats<0){
                        System.out.println("Introdueix una xifra positiva.");
                    }
                } while (unitats < 0);


                productesStoc.get(i).setUnitats(unitats+productesStoc.get(i).getUnitats()); // incrementem les unitats del producte
            }
        }
        if (trobat==false){
            estoc.addProducte(crearProducte(codi));
        }
    }






    /**
     * Afegeix un producte escollit per codi de barres al carro i el resta del nostre estoc
     * També comprova si ja hi ha un producte amb aquest codi, si fos així demana les unitats a incrementar
     * si no existeix clona el objecte del l'estoc per fer-ho servir al carro
     * @param codi codi de barres del producte
     */
    private static void afegirProducteCarro(String codi){

        ArrayList<Producte> productesEstoc = estoc.getCarro();
        ArrayList<Producte> productesCarro = carroActiu.getCarro();
        Producte prodEstoc = null;
        Producte prodCarro = null;

        boolean trobatE = false;
        for (int i=0; i<productesEstoc.size(); i++){
            if(codi.equalsIgnoreCase(productesEstoc.get(i).getCodiBarres())){
                trobatE = true;
                prodEstoc = productesEstoc.get(i);
            }
        }
        if (trobatE==false){       // No el tenim en estoc
            System.out.println("Aquet producte no el tenim!");
            Vista.pausarPantalla();
        } else {
            boolean trobatC = false;
            for (int i=0; i<productesCarro.size(); i++){
                if(codi.equalsIgnoreCase(productesCarro.get(i).getCodiBarres())){ // Hi es al estoc i al carro tambe
                    trobatC = true;
                    prodCarro = productesCarro.get(i);
                    System.out.println("Cuantes unitats vols mes?");

                    // demanem les unitats a afegir (si son negatives es tornen a demanar)
                    int unitats = 0;
                    do {
                        int[] tmp = null;
                        unitats = Validacio.demanarValidarEntrada(tmp);
                        if(unitats<0){
                            System.out.println("Introdueix una xifra positiva.");
                        } else if (unitats > prodEstoc.getUnitats()){
                            System.out.println("No tenim prou unitats a l'estoc");
                            System.out.println("Cuantes unitats vols?");
                            unitats = -1;
                        }
                    } while (unitats < 0);


                    prodEstoc.setUnitats(prodEstoc.getUnitats() - unitats);
                    prodCarro.setUnitats(prodCarro.getUnitats() + unitats);
                }
            }
            if (trobatC == false){    // No hi es al carro pero si al estoc
                System.out.println("Cuantes unitats vols?");

                // demanem les unitats a afegir (si son negatives es tornen a demanar)
                int unitats = 0;
                do {
                    int[] tmp = null;
                    unitats = Validacio.demanarValidarEntrada(tmp);
                    if(unitats<0){
                        System.out.println("Introdueix una xifra positiva.");
                    } else if (unitats > prodEstoc.getUnitats()){
                        System.out.println("No tenim prou unitats a l'estoc");
                        System.out.println("Cuantes unitats vols?");
                        unitats = -1;
                    }
                } while (unitats < 0);

                prodEstoc.setUnitats(prodEstoc.getUnitats() - unitats);
                Producte nouProducteCarro = clonarProducte(prodEstoc); // Si l'objecte no es troba en el carro fem un clon del que tenim a l'estoc
                nouProducteCarro.setUnitats(unitats);
                carroActiu.addProducte(nouProducteCarro);

            }
        }
    }







    /**
     * Fa una còpia d'un producte amb valors idèntics
     * PRECONDICIO: el producte no el tenim en el carro, per això hem de fer una copia l'objecte que tenim a l'estoc al carro
     * @param producte objecte producte passat per paràmetre
     * @return el nou objecte producte
     */
    private static Producte clonarProducte(Producte producte){
        Producte nou = null;
        if (producte instanceof Alimentacio){
            Alimentacio ali = (Alimentacio)producte;
            nou = new Alimentacio(ali.getCodiBarres(), ali.getNom(), ali.getPreuBase(), ali.getUnitats(), ali.getDataCad());
            nou.setCodiId(ali.getCodiId());
        }
        if (producte instanceof Electronic){
            Electronic elec = (Electronic)producte;
            nou = new Electronic(elec.getCodiBarres(), elec.getNom(), elec.getPreuBase(), elec.getUnitats(), elec.getGarantiaDies());
            nou.setCodiId(elec.getCodiId());
        }
        if (producte instanceof  Textil){
            Textil txt = (Textil)producte;
            nou = new Textil(txt.getCodiBarres(), txt.getNom(), txt.getPreuBase(), txt.getUnitats(), txt.getComposicio());
            nou.setCodiId(txt.getCodiId());
        }
        return nou;
    }











    /**
     * Demana a l'usuari les dades necessàries per crear un producte nou.
     * PRECONDICIO: s'ha de saber previament el seu codi de barres ficant-lo per paràmetre.
     * @param codi codi de barres
     */
    private static Producte crearProducte(String codi){

        Scanner in = new Scanner(System.in);
        Producte newProducte = null;

        String nom;
        double preuActual, preuBase;
        int unitats;



            System.out.println("De que es el teu producte?");

            switch (Vista.showMenuTipusProducte()){
                case 1: // Alimentacio
                    System.out.println("Introdueix el nom:");
                    nom = in.nextLine();
                    System.out.println("Introdueix les unitats:");
                    int []opcionsA = null;
                    unitats = Validacio.demanarValidarEntrada(opcionsA);
                    System.out.println("Introdueix el preu unitari:");
                    preuBase = Validacio.demanarValidarDecimal();
                    System.out.println("Introdueix data caducitat:");
                    String dataCad = Validacio.demanarValiarData();
                    while (dataCad.equalsIgnoreCase("0")){
                        dataCad = Validacio.demanarValiarData();
                    }
                    Alimentacio pAl = new Alimentacio(codi, nom, preuBase, unitats, dataCad);
                    newProducte = pAl;
                    break;
                case 2: // Textil
                    System.out.println("Introdueix el nom:");
                    nom = in.nextLine();
                    System.out.println("Introdueix les unitats:");
                    int[] opcionsT = null;
                    unitats = Validacio.demanarValidarEntrada(opcionsT);
                    System.out.println("Introdueix el preu unitari:");
                    preuBase = Validacio.demanarValidarDecimal();
                    System.out.println("Introdueix composicio:");
                    String composicio = in.nextLine();
                    Textil pTx = new Textil(codi, nom, preuBase, unitats, composicio);
                    newProducte = pTx;
                    break;
                case 3: // Electronic
                    System.out.println("Introdueix el nom:");
                    nom = in.nextLine();
                    System.out.println("Introdueix les unitats:");
                    int []opcionsE = null;
                    unitats = Validacio.demanarValidarEntrada(opcionsE);
                    System.out.println("Introdueix el preu unitari:");
                    preuBase = Validacio.demanarValidarDecimal();
                    System.out.println("Introdueix dies de garantia:");
                    int []opc = null;
                    int garantia = Validacio.demanarValidarEntrada(opc);
                    Electronic pEl = new Electronic(codi, nom, preuBase, unitats, garantia);
                    newProducte = pEl;
                    break;
                case 4:
                    break;
            }

        return newProducte;
    }













    /**
     * Resta les unitats d'un producte de l'estoc
     * @param codi codi del producte a eliminar
     */
    private static int treureProducteContenidor(Contenidor origen, String codi){
        boolean trobat = false;
        int unitats = 0;
        ArrayList<Producte> productes = origen.getCarro();
        for (int i=0; i<productes.size(); i++){
            Producte producte = productes.get(i);
            if (producte.getCodiBarres().equalsIgnoreCase(codi)){
                trobat = true;
                System.out.println("Tenim "+producte.getUnitats()+" unitats del producte "+producte.getCodiBarres()+" "+producte.getNom().toUpperCase());
                System.out.println("Cuantes vols eliminar?");

                // demanem les unitats a afegir (si son negatives es tornen a demanar)
                do {
                    int[] tmp = null;
                    unitats = Validacio.demanarValidarEntrada(tmp);
                    if(unitats<0){
                        System.out.println("Introdueix una xifra positiva.");
                    } else if (unitats > producte.getUnitats()){
                        System.out.println("És impossible eliminar més unitats de les que tens al carro!");
                        unitats = -1;
                    }
                } while (unitats < 0);

                producte.setUnitats(producte.getUnitats() - unitats);


            }
        }
        if (trobat==false){
            System.out.println("Ho sento el producte amb codi "+codi+" no s'ha trobat");
            Vista.pausarPantalla();
        }

        return unitats;
    }









    /**
     * Retorna unitats passades per paràmetre al producte amb el codi passat per paràmetre, directament sense interacció amb l'usuari
     * PRECONDICIO: Les mateixes unitats s'han eliminat prèviament del carro de l'usuari
     * @param codi codi producte a treure unitats
     * @param unitats unitats del producte a treure
     */
    private static void retornarProducteEstoc(String codi, int unitats){
        ArrayList<Producte> vEstoc = estoc.getCarro();
        for(int i=0; i<vEstoc.size(); i++){
            Producte p = vEstoc.get(i);
            if (p.getCodiBarres().equalsIgnoreCase(codi)){
                p.setUnitats(p.getUnitats()+unitats);
            }
        }
    }








    /**
     * Comprova que no hi hagi productes sense unitats en un contenidor, si troba cap, l'elimina
     * @param cont contenidor a comprovar
     */
    private static void eliminaProducteSenseUnitats(Contenidor cont){
        ArrayList<Producte> productes = cont.getCarro();
        for(int i=0; i<productes.size(); i++){
            if(productes.get(i).getUnitats() <= 0){
                productes.remove(i);
            }
        }
    }






}
