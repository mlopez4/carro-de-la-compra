/**
 * @author Marcos Lopez Capitan
 */

package model;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Classe per crear objectes contenidor on emmagatzemar els nostres productes
 */
public class Contenidor {


  /**
   * ArrayList s'emmagatzeman productes
   */
  private ArrayList<Producte> diposit = new ArrayList<Producte>();


  /**
   * Crea un contenidor de productes buit. (Carro)
   */
  public Contenidor (){

  }


  /**
   * Crea un contenidor de productes des de un fitxer csv desde la ruta passada per paràmetre
   * @param ruta directori on es troba el fitxer de productes (Estoc)
   */
  public Contenidor (String ruta){
    File fitxer = new File(ruta);
    try{
      Scanner in = new Scanner(fitxer);
      while(in.hasNext()){
        String[] line = in.nextLine().split(";");
        switch (line[1]){
          case "A":
            Alimentacio prdA = new Alimentacio(line[2], line[3], Double.parseDouble(line[4]), Integer.parseInt(line[5]), line[7]);
            prdA.setCodiId(Integer.parseInt(line[0]));
            diposit.add(prdA);
            break;
          case "E":
            Electronic prdE = new Electronic(line[2], line[3], Double.parseDouble(line[4]), Integer.parseInt(line[5]), Integer.parseInt(line[6]));
            prdE.setCodiId(Integer.parseInt(line[0]));
            diposit.add(prdE);
            break;
          case "T":
            Textil prdT = new Textil(line[2], line[3], Double.parseDouble(line[4]), Integer.parseInt(line[5]), line[8]);
            prdT.setCodiId(Integer.parseInt(line[0]));
            diposit.add(prdT);
            break;
        }
      }
    } catch (Exception e){
      System.out.println("Error llegint fitxer d'estoc");
    }

  }


  /**
   * "Matxaca" el fitxer csv on tenim emmagatzemat el nostre estoc amb l'estoc nou del nostre programa
   */
  public void actualitzarFitxerEstoc(String ruta){
    FileWriter fichero = null;
    PrintWriter pw = null;
    try
    {
      fichero = new FileWriter(ruta);
      pw = new PrintWriter(fichero);

      pw.println("CODI;CATEGORIA;CODI DE BARRES;NOM;PREU BASE;UNITATS;GARANTIA;DATA CAD;COMPOSICIO");


      for(int i=0; i<diposit.size(); i++){
        Producte p = diposit.get(i);
        if (p instanceof Alimentacio){
          Alimentacio a = (Alimentacio)p;
          pw.println(p.getCodiId() +  ";A;" + p.getCodiBarres() + ";" + p.getNom() + ";" + p.getPreuBase() + ";" + p.getUnitats() + ";;" + a.getDataCad() +";");
        }
        if (p instanceof Electronic){
          Electronic e = (Electronic) p;
          pw.println(p.getCodiId() + ";E;" + p.getCodiBarres() + ";" + p.getNom() + ";" + p.getPreuBase() + ";" + p.getUnitats() + ";"+ e.getGarantiaDies() +";;");
        }
        if (p instanceof Textil){
          Textil t = (Textil) p;
          pw.println(p.getCodiId() + ";T;" + p.getCodiBarres() + ";" + p.getNom() + ";" + p.getPreuBase() + ";" + p.getUnitats() + ";;;" + t.getComposicio());
        }
      }


    } catch (Exception e) {
      System.out.println("Error escribint fitxer");
    } finally {
      try {
        // Nuevamente aprovechamos el finally para
        // asegurarnos que se cierra el fichero.
        if (null != fichero)
          fichero.close();
      } catch (Exception e2) {
        System.out.println("Error escribint fitxer");
      }
    }
  }







  /**
   * Agrega un producte al carro (ArrayList)
   * @param pProducte Objecte Producte
   */
  public void addProducte(Producte pProducte){
    diposit.add(pProducte);
  }







  /**
   * Retorna un producte del carro a partir del codi passat per paràmetre, si no troba el codi, retorna null.
   * @param pCodiProducte codi de barres del producte a eliminar
   * @return Un objecte tipus Producte o null en cas de no trobar-lo
   */
  public Producte getProducte(String pCodiProducte){
    Producte rProducte = null;
    for (int i=0; i<diposit.size(); i++){
      if (pCodiProducte.equalsIgnoreCase(diposit.get(i).getCodiBarres())){
        rProducte = diposit.get(i);
      }
    }
    return rProducte;
  }








  /**
   * Retorna el carro sencer (ArrayList)
   * @return arraylist on es troba el carro
   */
  public ArrayList<Producte> getCarro() {
    return diposit;
  }


  /**
   * Retorna el número de productes que tenim en el carro
   * @return numero de productes en un enter
   */
  public int getSize(){
    return diposit.size();
  }







  /**
   * Borra un Producte del carro a partir del codi passat per paràmetre
   * @param refProducte codi de barres
   */
  public void removeProducte(String refProducte){
    for (int i=0; i<diposit.size(); i++){
      if (refProducte.equalsIgnoreCase(diposit.get(i).getCodiBarres())){
        diposit.remove(i);
      }
    }
  }



}
