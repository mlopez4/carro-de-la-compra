/**
 * @author Marcos Lopez Capitan
 */

package vista;


import controlador.Main;
import controlador.Validacio;
import model.*;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Classe estática on disposem de les funcions per mostrar menús i demanar dades a l'usuari
 */
public class Vista {







  /**
   * Canviant aquesta constant podem definir l'amplada dels menús
   */
  static final int AMP_PANTALLA = 80;















  /**
   * Printa per pantalla el menú principal
   * @return la opció escollida (ja validada)
   */
  public static int showMenuPrincipal(){
    int resultat = 0;
    int[] opcions = {1,2,3,4};
    do {
      printLine();
      System.out.println("1) GESTIO D'ESTOC");
      System.out.println("2) GESTIO CARRO");
      System.out.println("3) PASSAR PER CAIXA");
      System.out.println("4) SORTIR");
      printLine();
      resultat = Validacio.demanarValidarEntrada(opcions);
    } while (resultat == 0);
    return resultat;
  }











  /**
   * Printa per pantalla el menú de gestió de l'estoc
   * @return opció escollida
   */
  public static int showMenuGestioEstoc(){
    int resultat = 0;
    int[] opcions = {1,2,3};
    do {
      printLine();
      System.out.println("1) AFEGIR PRODUCTE");
      System.out.println("2) TREURE PRODUCTE");
      System.out.println("3) TORNAR");
      printLine();
      resultat = Validacio.demanarValidarEntrada(opcions);
    } while (resultat == 0);
    return resultat;
  }









  /**
   * Mostra per pantalla el menú per gestionar el carro
   * @return opció escollida
   */
  public static int showMenuGestioCarro(){
    int resultat = 0;
    int[] opcions = {1,2,3};
    do {
      printLine();
      System.out.println("1) AFEGIR PRODUCTE");
      System.out.println("2) BORRAR PRODUCTE");
      System.out.println("3) TORNAR");
      printLine();
      resultat = Validacio.demanarValidarEntrada(opcions);
    } while (resultat == 0);
    return resultat;
  }










  /**
   * Printa per pantalla el menú per introduir producte al carro
   * @return la opció escollida (ja validada)
   */
  public static int showMenuTipusProducte(){
    int resultat = 0;
    int[] opcions = {1,2,3,4};
    do {
      printLine();
      System.out.println("1) PRODUCTE ALIMENTACIO");
      System.out.println("2) PRODUCTE TEXTIL");
      System.out.println("3) PRODUCTE ELECTRONIC");
      System.out.println("4) TORNAR");
      printLine();
      resultat = Validacio.demanarValidarEntrada(opcions);
    } while (resultat == 0);
    return resultat;
  }











  /**
   * Calcula el total de cada línia, el general i printa el tiquet a pagar per pantalla, en cas que el carro estigui buit printa missatge advertència i retorna true
   * @param carro amb els productes a pagar.
   * @return false (I missatge de carro buit) si el carro está buit - true si te contingut.
   */
  public static boolean showTiquet(Contenidor carro){

    boolean printat = false;
    ArrayList<Producte> productes = carro.getCarro();

    if (productes.size()==0){
      System.out.println("El carro està buit!!");
      printat = false;
    } else {
      printat = true;
      Double totalTiket = 0.0;

      System.out.print(String.format("%15s", "CODI DE BARRES"));
      System.out.print(String.format("%15s", "NOM"));
      System.out.print(String.format("%15s", "UNITATS"));
      System.out.print(String.format("%15s", "PREU UNITARI"));
      System.out.print(String.format("%15s", "TOTAL LINIA"));
      System.out.println();

      for (int i=0; i<productes.size(); i++){

        Producte pActual = productes.get(i);
        System.out.print(String.format("%15s", pActual.getCodiBarres()));
        System.out.print(String.format("%15s", pActual.getNom()));
        int unitats = pActual.getUnitats();
        Double preuActual = pActual.getPreuActual();
        System.out.print(String.format("%15s", unitats));
        System.out.print(String.format("%15s", preuActual));
        Double totalLinia = (double)Math.round(preuActual * unitats * 100d) / 100d;
        System.out.print(String.format("%15s", totalLinia));
        totalTiket += totalLinia;


        System.out.println();

      }
      System.out.println();

      System.out.println(String.format("%60s%10s", "TOTAL", totalTiket));
    }


    return printat;

  }









  /**
   * Printa l'estoc en pantalla (Fitxer estoc.csv)
   */
  public static void showEstoc(Contenidor estoc){

    ArrayList<Producte>  productes = estoc.getCarro();

    printLine();
    printTitol("EL NOSTRE ESTOC");
    printLine();

    for (int i=0; i<estoc.getSize(); i++){
      Producte prod = productes.get(i);
      System.out.print(String.format("%15s", prod.getCodiBarres()));
      System.out.print(String.format("%15s", prod.getNom()));
      System.out.print(String.format("%15s", prod.getUnitats()));
      System.out.print(String.format("%15s", prod.getPreuBase()));
      System.out.println();
    }

}







  /**
   * Printa per pantalla el contingut del carro
   * @param carro
   */
  public static void showCarro (Contenidor carro){

    printLine();
    printTitol("EL TEU CARRO");
    printLine();
    if (carro.getSize() == 0){
      System.out.println("El carro està buit!!");
    } else {
      ArrayList<Producte> productesCarro = carro.getCarro();
      System.out.println(String.format("%15s%15s%15s", "CODI DE BARRES", "NOM", "UNITATS"));
      for (int i=0; i<productesCarro.size(); i++){
        Producte producte_tmp = productesCarro.get(i);
        System.out.println(String.format("%15s%15s%15s", producte_tmp.getCodiBarres(), producte_tmp.getNom(), producte_tmp.getUnitats()));
      }
    }
  }









  /**
   * Demana l'usuari una entrada per teclat amb la intenció de congelar la pantalla
   */
  public static void pausarPantalla(){
    Scanner in = new Scanner(System.in);
    System.out.println("- Prem ENTER per continuar -");
    in.nextLine();
  }














  // ****** START ******* LINIES, SEPARACIONS I TITOLS
  // (Funcions per fer separacions i titols als mateixos menus d'aquesta classe)





  /**
   * Printa una línia separadora
   */
  private static void printLine(){
    for (int i=0; i<AMP_PANTALLA; i++){
      System.out.print("*");
    }
    System.out.println();
  }






  /**
   * Mostra un títol amb el text entrat per paràmetre
   * @param text Titol a mostrar
   */
  private static void printTitol(String text){
    printLine();
    for (int i=0; i < ((AMP_PANTALLA - text.length()) / 2); i++){
      if (i==0){
        System.out.print("*");
      } else {
        System.out.print(" ");
      }
    }

    System.out.print(text);

    for (int i=0; i < ((AMP_PANTALLA - text.length()) / 2); i++){
      if (i==((AMP_PANTALLA - text.length()) / 2)-1){
        System.out.print("*");
      } else {
        System.out.print(" ");
      }
    }
    System.out.println();
    printLine();

  }









}
